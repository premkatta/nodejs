const express = require('express');
const fs = require('fs');
const bodyParser = require('body-parser');
const app = express();

app.use(bodyParser())

app.get('/api/', function (req, res) {
    console.log(res);
    let data = fs.readFileSync("dummy_json");
    // console.log(data);
    let myNotes = JSON.parse(data);
    res.send(myNotes);
})

app.post('/api/addNote', function (req, res) {
    console.log(req.body);
    let data = fs.readFileSync("dummy_json");
    // console.log(data);
    let myNotes = JSON.parse(data);
    myNotes.push({
        title: req.body.title,
        body: req.body.body
    })
    fs.writeFileSync('dummy_json', JSON.stringify(myNotes));
    res.send({
        msg: "Success",
        
    });
})
app.post('/api/removeNote', function (req, res) {
    console.log(req.body);
    let data = fs.readFileSync("dummy_json");
    // console.log(data);
    let myNotes = JSON.parse(data);
    // myNotes.pop({
    //     title: req.body.title,
    //     body: req.body.body
    // })
    let res1 = myNotes.filter(function (e){
        return e.title != req.body.title;
    })
    fs.writeFileSync('dummy_json', JSON.stringify(res1));
    res.send({
        msg: "Success"
    });
})
app.post('/api/findOneNote',function (req,res){
    let data = fs.readFileSync("dummy_json");
    let myNotes = JSON.parse(data);
    let res1 = myNotes.filter(function (e){
        return e.title == req.body.title;
    })
    fs.writeFileSync('dummy_json',JSON.stringify(res1));
    res.send({
        abc: "yes done"
    });
})
// Ctrl+Shift+P Debug:Toggle Auto Attach

app.get('/api/about', function (req, res) {
    res.send({
        msg: "Nasas"
    });
})

app.listen(3000, () => {
    console.log("Running on Port 3000 ");
});
