const express = require('express');
const fs = require('fs');
const bodyParser = require('body-parser');
const app = express();
const MongoClient = require("mongodb").MongoClient;
const url = "mongodb://localhost:27017";


app.use(bodyParser())

MongoClient.connect(url, function (err, client) {
    app.get('/mongo',function(req,res){
        client.db("New").collection("Heroes").find().toArray(function(err, docs) {
            console.log(docs);
            res.send(docs);
            // callback(docs);
          });
    })
    app.post('/addMongoDb', function (req,res){
       
        client.db("New").collection("Heroes").insertMany([req.body]).then(function(err, docs) {
            res.send({
                msg:"Done  "+  req.body.title
            })
        });
        // console.log(req.body);
    })
    app.post('/updateMongoDb', function (req,res){
        console.log(req.body);
        // var old ={ "title":req.body.title ,} 
        // var updated = {$set:{'title':req.body.updatedtitle}}
        // console.log(updated);
        // console.log(old);
        // var updated = {$set:{req.body}};
        // console.log(req.body.title);
        client.db("New").collection("Heroes").updateMany({title:req.body.title},{$set:{title:req.body.updatedtitle}}).then(function (err, docs){
            res.send({
                msg:"updated with"+" "+req.body.updatedtitle
            })
        });
    })
    app.post('/removeMongoDb', function (req, res){
        client.db("New").collection("Heroes").deleteOne({title:req.body.title}).then(function (err, docs){
            res.send({
                msg:"removed"+" "+req.body.title
            })
        })
    })
    app.get('/findOne', function (req,res){
        client.db("New").collection("Heroes").find({title:req.body.title}).toArray(function (err, docs){
            res.send({
                docs
            })
        })
    })
    app.post('/indexCollection', function (req, res){
        client.db("New").collection("Heroes").createIndex({'adad':1},null);
        res.send({
            msg:"indexes.."
        })
    })
});
// Ctrl+Shift+P Debug:Toggle Auto Attach

app.get('/api/about', function (req, res) {
    res.send({
        msg: "Nasas"
    });
})

app.listen(3000, () => {
    console.log("Running on Port 3000 ");
});
