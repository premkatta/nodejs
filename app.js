const os = require('os'); // Importing a prebuilt package
const fs = require('fs'); // file system
const _ = require('lodash'); //Importing npm package (lodash)
const yargs = require('yargs').argv;
// const express = require('express');


let arr = [1,2,3,4,5,6];
// console.log(arr.indexOf(10));
let dummy = [];
for (let i=0;i<arr.length;i++){
    if (dummy.indexOf(arr[i])<0){
        dummy.push(arr[i]);
    }
} 

// console.log("yargs"+yargs);
// console.log(process.argv);
// node app.js add --title ="First Note" --body = "First note in my application"
// node app.js remove --title="First Note"
// node app.js findAll 
// node app.js findOne --title="First Note"
// To get package.json do npm init

let data = fs.readFileSync("dummy_json");
// console.log("abc"+data);
let myNotes = JSON.parse(data);

if (process.argv[2]=='add'){
    console.log('create a Note for To Do Application');
    // let title = process.argv[3].split('=')[1];
    let title = yargs.title;
    let body = yargs.body;
    myNotes.push({
        title:title,
        body:body
    }) 
    // console.log(myNotes);
    fs.writeFileSync('dummy_json', JSON.stringify(myNotes));
} else if (process.argv[2] == 'remove') {
    console.log("Remove a Note");
    console.log(myNotes.length);
   var a = _.pull(myNotes, myNotes[myNotes.length - 1]);
    console.log(myNotes);
    fs.writeFileSync('dummy_json', JSON.stringify(myNotes));
} else if (process.argv[2] == 'findAll') {
    console.log("Find all Notes");
    console.log(myNotes);
    fs.writeFileSync('dummy_json', JSON.stringify(myNotes));
} else if (process.argv[2] == 'findOne') {
    console.log("Find a Note");
    // console.log(process.argv[3]);
    // var myfind = _.find(myNotes ,process.argv[3] );
    // if (myfind=="undefined"){
    //     console.log("whatevr u r finding is not available");
    // }
    let title = yargs.title;
    let res = myNotes.filter(function (e){
        return e.title!=title;
    })
    // console.log(res);
    // JSON.stringify(res);
    // var a = _.pull(myNotes, res);
    // delete myNotes.res;
    console.log("findOne" ,res);
    // console.log(myfind);
}


// fs.writeFileSync('dummy_json', JSON.stringify(os.userInfo()));
// console.log(dummy);
// console.log(_.uniq(arr));
// console.log(os.userInfo());

//To run a node application you have to use command -----> node filename.js
/*
Advantages:
1.Asynchronous NonBloscking IO
2.High perfotmance for web applocations.
3.Single Threaded


*/